NASLOV: Roboti prevzemajo svet :S

CILJNA PUBLIKA: dijaki in študenti

OPIS: Na delavnici se bomo spoznali z programiranjem avtonomnih mobilnih robotov. S pomočjo mentorjev se bomo seznanili tako z osnovami mobilne robotike kot tudi z zahtevnejšimi problemi le teh. Pred zaključkom bo lahko vsak udeleženec svoj program lahko testiral tudi na realnem robotu.

VSEBINA:
 - Osnove ROS-a in Osnove vodenja robotov (Robot operating system - sistem ki omogoča hitrejši razvoj robotov v praksi) (Osnovni pojmi in pristopi pri vodenju mobilnih robotov)  
 - Reševanje navigacijskega problema v robotiki (Kako rešimo problem sledenja navidezni črti z mobilnim robotom)
 - Testiranje sprogramiranega v praksi (Vsak udeleženec dobi priložnost pogona svojega dela tudi na TAFR kmetijskem robotu)