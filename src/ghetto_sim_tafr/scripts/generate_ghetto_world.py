import cv2
import math
import numpy as np
import sys

##########################################################
# Generate path with plants and points from a map image
##########################################################

def main():
    # CHANGE THIS - World parameters in meters
    world_width = 10.0
    world_height = 10.0
    object_separation_radius = 0.5


    args = sys.argv
    
    if(len(args) == 1):
        file_path = "../data/map.png"
    elif(len(args) == 2):
        file_path = args[1]
    if(len(args) > 2):
        print("Too many arguments!")
        print("Usage:")
        print("      python generate_ghetto_world.py path_to_map_image")
        
        return

    try: 
        img  = cv2.imread(file_path, 1) 
    except IOError:
        print("No image of the world found. Add a map image to the folder /data")
    
    plants = []
    points = []
    start_pos = []

    # img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
    im_rows, im_cols, im_ch = img.shape

    # convert parameters to pixel values
    rad = object_separation_radius/world_width*im_cols
    print(rad)

    plant_channel,start_channel,point_channel = cv2.split(img)

    # cv2.imshow('R',point_channel)
    # cv2.imshow('G',start_channel)
    # cv2.imshow('B',plant_channel)
    # cv2.waitKey(0)

    # PLANT PLANTS:
    for c in range(0, im_cols):
        for r in range(0, im_rows):
            pixel_val = plant_channel[r, c]
            # Check if pixel value is approximately black
            if pixel_val > 200:
                if check_for_neighbours([r, c], plants, rad) > 0:
                    plants.append([r, c])



    # PLANT BEERS: 
    points = return_point_centers(point_channel) 
    # print(beers)

    start_pos = return_point_centers(start_channel)  
    # print(start_pos)

    points = reorder_points(points, start_pos)

    # plot plant and points positions
    for p in plants:
        cv2.circle(img, (p[1], p[0]), int(rad), (200, 125, 50), 1)
    i = 1
    for p in points:
        cv2.circle(img, (p[0], p[1]), 10, (100, 225, 50), 2)
        cv2.putText(img, "{}".format(i), (p[0] - 10, p[1] - 10),cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
        i+=1

    # Transform from pixel to world coordinates:
    plants_world = []
    for p in plants:
        x = world_height/2 - float(p[0])/im_rows*world_height
        y = world_width/2 - float(p[1])/im_cols*world_width

        plants_world.append([x, y])

    points_world = []
    for p in points:
        y = world_height/2 - float(p[0])/im_rows*world_height
        x = world_width/2 - float(p[1])/im_cols*world_width

        points_world.append([x, y])

    start_pos_world = []
    for p in start_pos:
        y = world_height/2 - float(p[0])/im_rows*world_height
        x = world_width/2 - float(p[1])/im_cols*world_width

        start_pos_world.append([x, y])

    # Generate yaml file for points
    f_points = open("../config/points.yaml", "w")
    i = 1
    for point in points_world:
        points_command = """
        point{}: {}
            'x': {},
            'y': {}
        {}

        """.format(i, "{", point[0], point[1], "}")
        i += 1
        f_points.write(points_command)
    f_points.close()


    # Generate yaml file for plants
    f_plants = open("../config/obstacles.yaml", "w")
    i = 1
    for plant in plants_world:
        plant_command = """
        plant{}: {}
            'x': {},
            'y': {}
        {}

        """.format(i, "{", plant[0], plant[1], "}")
        i += 1
        f_plants.write(plant_command)
    f_plants.close()

    # Generate yaml file for start position
    f_start = open("../config/start.yaml", "w")
    i = 1
    for start in start_pos_world:
        start_command = """
        start{}: {}
            'x': {},
            'y': {}
        {}

        """.format(i, "{", start[0], start[1], "}")
        i += 1
        f_start.write(start_command)
    f_start.close()


    cv2.imshow('image',img)
    cv2.waitKey(0)
    


def check_for_neighbours(new_point, list_of_points, rad):
    for point in list_of_points:
        d = math.sqrt( (point[0] - new_point[0])**2 + (point[1] - new_point[1])**2)
        # print("beer,  d: ", d)
        if d < rad:
            return -1
    return 1

def return_point_centers(img):
    ret, img_points = cv2.threshold(img,180,255,cv2.THRESH_BINARY)
    # img_points = cv2.bitwise_not(img_points)
    kernel = np.ones((5,5),np.uint8)
    img_points = cv2.morphologyEx(img_points, cv2.MORPH_CLOSE, kernel)

    points = []
    # find contours in the binary image
    img_points, contours, hierarchy = cv2.findContours(img_points,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    for c in contours:
        # calculate moments for each contour
        M = cv2.moments(c)

        # calculate x,y coordinate of center
        if M["m00"] != 0:
            cX = int(M["m10"] / M["m00"])
            cY = int(M["m01"] / M["m00"])
        
            points.append([cX, cY])

    return points


def reorder_points(points, start_pos):
    points_ordered = []
    points_list = points
    curr_point = start_pos[0] 
    for i in range(0, len(points)):
        dist = [(math.sqrt( (b[0] - curr_point[0])**2 + (b[1] - curr_point[1])**2)) for b in points_list]
        idx =  dist.index(min(dist)) 
        points_ordered.append(points_list[idx])
        points_list.pop(idx)
        curr_point = points_ordered[-1]
        
    return points_ordered


if __name__ == '__main__':
    main()