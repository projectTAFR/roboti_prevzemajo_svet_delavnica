#include <geometry_msgs/TransformStamped.h>
#include <std_msgs/Bool.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_ros/transform_broadcaster.h>
#include <nav_msgs/Odometry.h>
//#include <opencv-3.3.1-dev/opencv2/core/types.hpp>
#include <sensor_msgs/PointCloud.h>
//#include <opencv-3.3.1-dev/opencv2/opencv.hpp>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/opencv.hpp"

#define TIME_STEP 0.05
#define IMG_SIZE 800
#define PX_PER_METER 80.0

#define ROBOT_WIDTH 0.40

#define NOISE 0.02

struct Obstacle{
	double x;
	double y;
	double score;
	cv::Scalar color;
	Obstacle(double x_,double y_,double s_, cv::Scalar c_) : x(x_),y(y_),score(s_), color(c_){}
};

class GhettoSimTAFR {
	ros::Subscriber cmdvel_sub;
	ros::Subscriber cmdvel_sub2;
	ros::Subscriber timer_sub;
	ros::Publisher pub_odom;
	ros::Publisher pub_obstacles;
	ros::Timer timer;
	tf2_ros::TransformBroadcaster odom_broadcaster;
	bool headless=false;
	bool stop_timer = false;
	double cmd_lin_x = 0, cmd_ang_z = 0;

	double pose_x = 0, pose_y = 0, pose_th = 0;

	int num_of_hits = 0;

	ros::Time last_time_cmdvel;
	double start_time = ros::Time::now().toSec();

	std::vector<Obstacle> obstacle_list = {
//			Obstacle(1,1,10)
//			Obstacle(2,1.1,15),
//			Obstacle(3,1.1,15),
//			Obstacle(4,1,15),
//			Obstacle(5,0.9,15),
//			Obstacle(3,-1,15),
//			Obstacle(1,1,15)
	};
	std::vector<Obstacle> point_list;

public:
	GhettoSimTAFR() {
		ros::NodeHandle n;
		ros::NodeHandle np("~");
		np.getParam("headless", headless);
		np.getParam("x", pose_x);
		np.getParam("y", pose_y);
		ROS_INFO_STREAM("runninng in headless mode:"<<headless);

		placeRobotAtStartLocation();

		getPointsPosesFromParams();
		getPlantPosesFromParams();

		pub_odom = n.advertise<nav_msgs::Odometry>("odom", 4);
		pub_obstacles = n.advertise<sensor_msgs::PointCloud>("laser/lidar_cf/lidar_clusters", 1);
		cmdvel_sub = n.subscribe("cmd_vel", 10, &GhettoSimTAFR::cmdvelCB, this);
		cmdvel_sub2 = n.subscribe("auto_cmd_vel", 10, &GhettoSimTAFR::cmdvelCB, this);
		timer_sub = n.subscribe("path_end", 10, &GhettoSimTAFR::stopTimerCB, this);
		timer = n.createTimer(ros::Duration(TIME_STEP), &GhettoSimTAFR::timerCallback, this);
	}



private:

	void getPointsPosesFromParams() {
		ROS_INFO("Fetching points poses from parameter server");
		for (int i = 0; i < 256; i++) {
			std::string param_name = "point" + std::to_string(i);
			ros::NodeHandle np;
			if (np.hasParam(param_name)) {
				std::map<std::string, double> pose_param;
				np.getParam(param_name, pose_param);
//				tf::Transform m_tf(tf::Quaternion(pose_param["qx"], pose_param["qy"], pose_param["qz"], pose_param["qw"]),
//								   tf::Vector3(pose_param["x"], pose_param["y"], pose_param["z"]));
//				true_marker_poses[std::to_string(i)] = m_tf;
				point_list.push_back(Obstacle(pose_param["x"], pose_param["y"],0,cv::Scalar(0,0,255)));
				ROS_INFO_STREAM("Added point " << i << " at: " << pose_param["x"] << " " << pose_param["y"] << " ");

			}
		}
		ROS_INFO("DONE Fetching points from parameter server");
	}
	void getPlantPosesFromParams() {
		ROS_INFO("Fetching plant poses from parameter server");
		for (int i = 0; i < 512; i++) {
			std::string param_name = "plant" + std::to_string(i);
			ros::NodeHandle np;
			if (np.hasParam(param_name)) {
				std::map<std::string, double> pose_param;
				np.getParam(param_name, pose_param);
//				tf::Transform m_tf(tf::Quaternion(pose_param["qx"], pose_param["qy"], pose_param["qz"], pose_param["qw"]),
//								   tf::Vector3(pose_param["x"], pose_param["y"], pose_param["z"]));
//				true_marker_poses[std::to_string(i)] = m_tf;
				obstacle_list.push_back(Obstacle(pose_param["x"], pose_param["y"],0, cv::Scalar(0, 200, 0)));
				ROS_INFO_STREAM("Added plant " << i << " at: " << pose_param["x"] << " " << pose_param["y"] << " ");

			}
		}
		ROS_INFO("DONE Fetching plants from parameter server");
	}

	void placeRobotAtStartLocation(){
		ROS_INFO("Fetching start pose from parameter server");
		std::string param_name = "start1";
		ros::NodeHandle np;
		if (np.hasParam(param_name)) {
			std::map<std::string, double> pose_param;
			np.getParam(param_name, pose_param);
			pose_x = pose_param["x"];
			pose_y = pose_param["y"];
		}
	}

	void timerCallback(const ros::TimerEvent &) {
		if(ros::Time::now().toSec()-last_time_cmdvel.toSec()<0.3){
			cmdVelToPose();
		}
		pubOdomAndTF();
		pubObstacles();

		if(!headless){
			cv::Mat img(IMG_SIZE, IMG_SIZE, CV_8UC3, cv::Scalar(255, 255, 255));

			

			drawObstacles(img);
			drawPoints(img);
			drawOriginAndGrid(img);
			drawRobot(img);
			drawTime(img);
			drawScore(img);

			checkForHits(img);

			cv::imshow("sim TAFR", img);
			cv::waitKey(1);
		}

	}

	void checkForHits(cv::Mat &img){
		int robot_width_px = ROBOT_WIDTH * PX_PER_METER;

		double angle = worldAngleToImg(pose_th);
		cv::Point robot_pose = mPointToImgPoint(pose_x, pose_y);
		cv::Point circle_center = robot_pose - cv::Point(robot_width_px / 2 * cos(angle), robot_width_px / 2 * sin(angle));

		// Plot hit zones
		// cv::circle(img, circle_center, robot_width_px / 2, cv::Scalar(255, 0, 0), CV_FILLED, 8, 0);
		// cv::circle(img, robot_pose, robot_width_px / 2, cv::Scalar(250, 0, 0), CV_FILLED, 8, 0);

		int i = 0;
		for(Obstacle obst : obstacle_list){
			cv::Point obs_pos = mPointToImgPoint(obst.x, obst.y);
			float d1 = cv::norm(robot_pose - obs_pos);
			float d2 = cv::norm(circle_center - obs_pos);

			if(d1 < robot_width_px/2 || d2 < robot_width_px/2){
				obstacle_list[i].color = cv::Scalar(255, 0, 255);
				if(!obstacle_list[i].score){
					obstacle_list[i].score = 1;
					num_of_hits++;
				}
			}
			i++;
		}

	}

	void stopTimerCB(const std_msgs::Bool::ConstPtr &msg){
		stop_timer = msg->data;
	}

	void cmdvelCB(const geometry_msgs::Twist::ConstPtr &msg) {
		cmd_lin_x = msg->linear.x;
		cmd_ang_z = msg->angular.z;
		last_time_cmdvel = ros::Time::now();
	}

	void cmdVelToPose() {
		double v_x = cmd_lin_x * cos(pose_th);
		double v_y = cmd_lin_x * sin(pose_th);
		double v_th = cmd_ang_z;

		pose_x += v_x * TIME_STEP;
		pose_y += v_y * TIME_STEP;
		pose_th += v_th * TIME_STEP;
	}

	void pubOdomAndTF() {
		auto current_time = ros::Time::now();
		tf2::Quaternion odom_quat;
		odom_quat.setRPY(0, 0, pose_th);
		//publish the transform over tf
		geometry_msgs::TransformStamped odom_trans;
		odom_trans.header.stamp = current_time;
		odom_trans.header.frame_id = "odom";
		odom_trans.child_frame_id = "base_link";
		odom_trans.transform.translation.x = pose_x;
		odom_trans.transform.translation.y = pose_y;
		odom_trans.transform.translation.z = 0.0;
		odom_trans.transform.rotation.x = odom_quat.x();
		odom_trans.transform.rotation.y = odom_quat.y();
		odom_trans.transform.rotation.z = odom_quat.z();
		odom_trans.transform.rotation.w = odom_quat.w();
		odom_broadcaster.sendTransform(odom_trans); //send the transform

		nav_msgs::Odometry odom;
		odom.header.stamp = current_time;
		odom.header.frame_id = "odom";
		odom.pose.pose.position.x = pose_x;
		odom.pose.pose.position.y = pose_y;
		odom.pose.pose.position.z = 0.0;
		odom.pose.pose.orientation.x = odom_quat.x();
		odom.pose.pose.orientation.y = odom_quat.y();
		odom.pose.pose.orientation.z = odom_quat.z();
		odom.pose.pose.orientation.w = odom_quat.w();
		odom.child_frame_id = "base_link";
//		odom.twist.twist.linear.x = v_x;
//		odom.twist.twist.linear.y = v_y;
//		odom.twist.twist.angular.z = v_th;
		odom.pose.covariance[0] = 0.01;
		odom.pose.covariance[7] = 0.01;
		odom.pose.covariance[14] = 0;
		odom.pose.covariance[21] = 0;
		odom.pose.covariance[28] = 0;
		odom.pose.covariance[35] = 0.01;
		pub_odom.publish(odom);
	}


	void pubObstacles(){
		sensor_msgs::PointCloud pcl;
		pcl.header.frame_id = "base_link";
		geometry_msgs::Point32 pt;
		for(Obstacle obst : obstacle_list){
//			pt.x =  cos(pose_th)*obst.x +sin(pose_th)*obst.y - pose_x;
//			pt.y = -sin(pose_th)*obst.x +cos(pose_th)*obst.y - pose_y;
//			pt.x = (obst.x - pose_x)*cos(pose_th);
//			pt.y = (obst.y - pose_y)*sin(pose_th);
//			pt.x = cos(pose_th)*pose_x + obst.x;
//			pt.y = sin(pose_th)*pose_y + obst.y;
			double dx = obst.x - pose_x;
			double dy = obst.y - pose_y;
			pt.x = cos(-pose_th) * dx - sin(-pose_th) * dy;
			pt.y = cos(-pose_th) * dy + sin(-pose_th) * dx;

			if(pt.x >0){ // if point would be visible by front-facing lidar
				pt.x+=((double) rand() / (RAND_MAX))*NOISE;
				pt.y+=((double) rand() / (RAND_MAX))*NOISE;
				pcl.points.push_back(pt);
			}

		}
		pub_obstacles.publish(pcl);
	}


	// DRAWING FUNCTIONS

	int mToPx(double m) {
		return (int) (m * PX_PER_METER);
	}

	double pxToM(int px) {
		return px / PX_PER_METER;
	}

	double worldAngleToImg(double a) {
		return -a + 3.14159 / 2;
	}

	cv::Point mPointToImgPoint(double x, double y) {
		cv::Point pt;
		pt.x = IMG_SIZE * 0.5 - mToPx(y);
		pt.y = IMG_SIZE * 0.5 - mToPx(x);
		return pt;
	}

	void drawRobot(cv::Mat &img) {
		int robot_width_px = ROBOT_WIDTH * PX_PER_METER;

		double angle = worldAngleToImg(pose_th);
		cv::Point robot_pose = mPointToImgPoint(pose_x, pose_y);
		cv::Point circle_center = robot_pose - cv::Point(robot_width_px / 2 * cos(angle), robot_width_px / 2 * sin(angle));
		drawRotatedRectangle(img, robot_pose, cv::Size(robot_width_px, robot_width_px), angle / 3.14159 * 180.0);
		cv::circle(img, circle_center, robot_width_px / 2, cv::Scalar(0, 0, 255), CV_FILLED, 8, 0);
		cv::circle(img, robot_pose, 4, cv::Scalar(250, 0, 255), CV_FILLED, 8, 0);
//		std::cout << circle_center << "  "<< robot_pose << std::endl;
	}

	void drawRotatedRectangle(cv::Mat &image, cv::Point centerPoint, cv::Size rectangleSize, double rotationDegrees) {
		cv::Scalar color = cv::Scalar(0, 0, 255);

		// Create the rotated rectangle
		cv::RotatedRect rotatedRectangle(centerPoint, rectangleSize, rotationDegrees);

		// We take the edges that OpenCV calculated for us
		cv::Point2f vertices2f[4];
		rotatedRectangle.points(vertices2f);

		// Convert them so we can use them in a fillConvexPoly
		cv::Point vertices[4];
		for (int i = 0; i < 4; ++i) {
			vertices[i] = vertices2f[i];
		}

		// Now we can fill the rotated rectangle with our specified color
		cv::fillConvexPoly(image, vertices, 4, color);
	}

	void drawObstacles(cv::Mat &img){
		for(Obstacle obst : obstacle_list){
			cv::circle(img,cv::Point(mPointToImgPoint(obst.x,obst.y)),8,obst.color,-1);
		}
	}
	void drawPoints(cv::Mat &img){
		for(Obstacle obst : point_list){
			cv::circle(img,cv::Point(mPointToImgPoint(obst.x,obst.y)),8,obst.color,-1);
		}
	}
	void drawTime(cv::Mat &img){
		static std::string t = "";
		if(!stop_timer){
			t =  "Time: " + std::to_string(ros::Time::now().toSec() - start_time);	
		}
		cv::putText(img, t, cvPoint(IMG_SIZE/10*0.5,30), cv::FONT_HERSHEY_PLAIN, 2, cvScalar(0,0,0), 1, CV_AA);
	}
	void drawScore(cv::Mat &img){
		std::string t = "Number of hits: ";
		t += std::to_string(num_of_hits);
		cv::putText(img, t, cvPoint(IMG_SIZE/10*5,30), cv::FONT_HERSHEY_PLAIN, 2, cvScalar(0,0,0), 1, CV_AA);
	}

	void drawOriginAndGrid(cv::Mat &img){
		cv::line(img,mPointToImgPoint(1,0),mPointToImgPoint(0,0),cv::Scalar(0,0,255)); // up  - x axis
		cv::line(img,mPointToImgPoint(0,1),mPointToImgPoint(0,0),cv::Scalar(0,255,0)); // left - y axis

		for(int x= -5;x<=5;++x){
			for(int y=-5;y<=5;++y){
				cv::circle(img,mPointToImgPoint(x,y),1,cv::Scalar(150,150,150));
			}
		}
	}
};


int main(int argc, char **argv) {
	ros::init(argc, argv, "ghetto_tafr_simulation");
	GhettoSimTAFR mc;
	ros::spin();
	return 0;
}