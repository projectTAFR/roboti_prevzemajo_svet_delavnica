#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy, math
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Twist
from std_msgs.msg import Bool
from tf.transformations import euler_from_quaternion, quaternion_from_euler

def sledenje_tockam():
# spin() simply keeps python from exiting until this node is stopped
	rospy.spin()


def odom_callback(msg):
	global current_path_index

	if(current_path_index>=len(path)):
		rospy.loginfo("Robot has reached the end of the awesome path")

		end_timer = Bool()
		end_timer.data = True
		time_stop_pub.publish(end_timer)
		return

	robot_x = msg.pose.pose.position.x
	robot_y = msg.pose.pose.position.y

	quaternion = (
	msg.pose.pose.orientation.x,
	msg.pose.pose.orientation.y,
	msg.pose.pose.orientation.z,
	msg.pose.pose.orientation.w
	)

	euler = euler_from_quaternion(quaternion)
	robot_yaw = euler[2]

	kotna_hitrost = 0
	linearna_hitrost = 0
	########################################################################################################
	#                           TUKAJ PIŠI KODO
	########################################################################################################
	# Vhodni podatki so: robot_x, robot_y (pozicija robota), robot_yaw (zasuk robota v radianih)
	# Poračunati morate linearno in kotno hitrost


	########################################################################################################
	#                           TUKAJ NEHAJ PISATI KODO
	########################################################################################################
	max_lin_vel = 2
	max_ang_vel = 1
	if abs(linearna_hitrost) > max_lin_vel:
		linearna_hitrost = max_lin_vel*linearna_hitrost/abs(linearna_hitrost)
	if abs(kotna_hitrost) > max_ang_vel:
		kotna_hitrost = max_ang_vel*kotna_hitrost/abs(kotna_hitrost)


	cmd_vel = Twist()
	cmd_vel.linear.x = linearna_hitrost
	cmd_vel.angular.z = kotna_hitrost
	cmd_pub.publish(cmd_vel)

def wrap_to_pi(angle):
	return math.atan2(math.sin(angle), math.cos(angle))


if __name__ == '__main__':
	rospy.init_node('sledenje_tockam', anonymous=True)
	odom_sub = rospy.Subscriber("odom", Odometry , odom_callback)
	cmd_pub = rospy.Publisher("cmd_vel", Twist, queue_size=5)
	time_stop_pub = rospy.Publisher("path_end", Bool, queue_size=5)

	path = []
	current_path_index = 0
	for i in range(1,255):
		if rospy.has_param("point"+str(i)):
			point = rospy.get_param("point"+str(i))
			point_x = point["x"]
			point_y = point["y"]
			path.append([point_x, point_y])

	print(path) 

	sledenje_tockam()